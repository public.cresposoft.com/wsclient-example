# Wsclient Example
Start the web service:

    start-ws-server.bat

Build the jar file:

    mvn clean package

Run the web service client:

    start-ws-client.bat

It should be return the following output:
    
    GetCountryResponse{
        country=Country{
            name='Spain',
            population=46704314,
            capital='Madrid',
            currency=EUR
        }
    }
    GetCountryResponse{
        country=Country{
            name='Poland',
            population=38186860,
            capital='Warsaw',
            currency=PLN
        }
    }
    GetCountryResponse{
        country=Country{
            name='United Kingdom',
            population=63705000,
            capital='London',
            currency=GBP
        }
    }
