package com.baeldung.soap.ws.client;

import java.net.MalformedURLException;
import java.net.URL;

import io.spring.guides.gs_producing_web_service.CountriesPort;
import io.spring.guides.gs_producing_web_service.CountriesPortService;
import io.spring.guides.gs_producing_web_service.Country;
import io.spring.guides.gs_producing_web_service.GetCountryRequest;
import io.spring.guides.gs_producing_web_service.GetCountryResponse;

public class CountriesServiceClient {

    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("http://localhost:8080/ws/countries.wsdl");
        CountriesPortService service = new CountriesPortService(url);
        CountriesPort port = service.getCountriesPortSoap11();
        System.out.println(countryResponseToString(getCountry(port, "Spain")));
        System.out.println(countryResponseToString(getCountry(port, "Poland")));
        System.out.println(countryResponseToString(getCountry(port, "United Kingdom")));
    }

    private static Country getCountry(CountriesPort port, String countryName) {
        GetCountryRequest request = new GetCountryRequest();
        request.setName(countryName);
        GetCountryResponse country = port.getCountry(request);
        return country.getCountry();
    }

    private static String countryResponseToString(Country countryInfo) {
        return "GetCountryResponse{\n" +
                "    country=" + "Country{\n" +
                "        name='" + countryInfo.getName() + '\'' + ",\n" +
                "        population=" + countryInfo.getPopulation() + ",\n" +
                "        capital='" + countryInfo.getCapital() + '\'' + ",\n" +
                "        currency=" + countryInfo.getCurrency() + "\n" +
                "    }" + "\n" +
                "}";
    }
}